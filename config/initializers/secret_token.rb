# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
LabGrader::Application.config.secret_key_base = '6df3ed754e9feab6c29e0ecbccc8c210d8b9be2c7761112d9adabf4eab564f349aa874b0c54d53d405ce103a240d375342edd7b06a54860cff7c2afabf3f72b9'
