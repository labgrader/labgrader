LabGrader::Application.routes.draw do

  #------------------------------------Application Controller------------------------------------
  #get "application/login"
  #get "application/logout"
  #get 'login', to: 'application#login'
  #get 'privilegeCheck', to: 'application#privilegeCheck'
  #resources :appplication, only: [:logout] do
  #  collection do
  #    post :logout
  #  end
  #end
 
  match '/login' => 'application#login', via: [:get, :post, :patch], :as => :login
  match '/logout' => 'application#logout', via: [:get, :post, :patch], :as => :logout
  match '*section/logout' => 'application#logout', via: [:get, :post, :patch]
  
  #------------------------------------Home Controller------------------------------------ 
  #get "home/index" 
  #get "home/qr" 
  #get "home/create_account"
  root :to => 'home#index'
  match '/index' => 'home#index', via: [:get, :post, :patch], :as => :home
  match '/qr' => 'home#qr', via: [:get, :post, :patch], :as => :qr
  match '/create_account' => 'home#create_account', via: [:get, :post, :patch], :as => :create_account

  #------------------------------------Dashboard Controller------------------------------------
  match '/dashboard' => 'dashboard#index', via: [:get, :post, :patch], as: :dashboard
  match '/dashboard/import' => 'dashboard#import', via: :post, as: :import
  match '/dashboard/add_col' => 'dashboard#add_col', via: :post, as: :add_col
  match '/dashboard/remove_col' => 'dashboard#remove_col', via: :post, as: :remove_col 
  match '/dashboard/remove_student/:id' => 'dashboard#remove_student', via: [:post, :delete], as: :remove_student
  match '/dashboard/set_default_lab' => 'dashboard#set_default_lab', via: :post, as: :set_default
  match '/dashboard/mass_qr' => 'dashboard#mass_qr', via: [:get, :post], as: :mass_qr
  get 'dashboard/update' => redirect('/update')
  match '/update' => 'dashboard#index', via: [:get, :post, :patch], :as => :update
  match '/title' => 'dashboard#title', via: [:get, :post, :patch], :as => :title

  #------------------------------------Logger Controller------------------------------------
  #post '/logger' => 'site#save_grade', :as => :save_grade
  #post '/admin' => 'site#create_product', :as => :create_product
  #resources :logger, only: [:index, :save_grade]
  match '*section/save_grade' => 'logger#save_grade', via: [:get, :post, :patch], as: :save_grade
  match '/save_grade'=> 'logger#save_grade', via: [:get, :post, :patch]
  match '/logger' => 'logger#index', via: [:get, :post, :patch], :as => :logger 


  #------------------------------------Account Management-----------------------------------
  match '/manage' => 'account_management#index', via: [:get, :post, :patch], :as => :account_management


  #------------------------------------Lost Account-----------------------------------------
  match '/lostaccount' => 'lost_account#index', via: [:get, :post, :patch], :as => :lost_account

  #------------------------------------Emailer Controller-----------------------------------------
  match '/sendmail/:id' => 'emailer#sendmail', via: [:get, :post], as: :send_mail
  # The priority is based upon order of creation: first created -> highest priority.
  
  
  #------------------------------------Admin Settings------------------------------------------------
  match '/settings' => 'settings#index', via: [:get, :post], as: :settings
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
