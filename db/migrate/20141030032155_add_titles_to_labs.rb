class AddTitlesToLabs < ActiveRecord::Migration
  def change
    add_column :labs, :lab01_title, :string
    add_column :labs, :lab02_title, :string
    add_column :labs, :lab03_title, :string
    add_column :labs, :lab04_title, :string
    add_column :labs, :lab05_title, :string
    add_column :labs, :lab06_title, :string
    add_column :labs, :lab07_title, :string
    add_column :labs, :lab08_title, :string
    add_column :labs, :lab09_title, :string
    add_column :labs, :lab10_title, :string
    add_column :labs, :lab11_title, :string
    add_column :labs, :lab12_title, :string
    add_column :labs, :lab13_title, :string
    add_column :labs, :lab14_title, :string
    add_column :labs, :lab15_title, :string
    add_column :labs, :lab16_title, :string
    add_column :labs, :lab17_title, :string
    add_column :labs, :lab18_title, :string
    add_column :labs, :lab19_title, :string
    add_column :labs, :lab20_title, :string
    add_column :labs, :lab21_title, :string
    add_column :labs, :lab22_title, :string
    add_column :labs, :lab23_title, :string
    add_column :labs, :lab24_title, :string
    add_column :labs, :lab25_title, :string
    add_column :labs, :lab26_title, :string
    add_column :labs, :lab27_title, :string
    add_column :labs, :lab28_title, :string
    add_column :labs, :lab29_title, :string
    add_column :labs, :lab30_title, :string
  end
end
