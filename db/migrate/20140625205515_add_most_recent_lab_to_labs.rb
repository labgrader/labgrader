class AddMostRecentLabToLabs < ActiveRecord::Migration
  def change
    add_column :labs, :most_recent_lab, :integer
  end
end
