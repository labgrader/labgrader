class AddClassIdToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :class_id, :string
  end
end
