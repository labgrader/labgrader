class AddMoreLabsToStudents < ActiveRecord::Migration
  def change
    add_column :students, :lab10, :float
    add_column :students, :lab11, :float
    add_column :students, :lab12, :float
    add_column :students, :lab13, :float
    add_column :students, :lab14, :float
    add_column :students, :lab15, :float
    add_column :students, :lab16, :float
    add_column :students, :lab17, :float
    add_column :students, :lab18, :float
    add_column :students, :lab19, :float
    add_column :students, :lab20, :float
    add_column :students, :lab21, :float
    add_column :students, :lab22, :float
    add_column :students, :lab23, :float
    add_column :students, :lab24, :float
    add_column :students, :lab25, :float
    add_column :students, :lab26, :float
    add_column :students, :lab27, :float
    add_column :students, :lab28, :float
    add_column :students, :lab29, :float
    add_column :students, :lab30, :float
  end
end
