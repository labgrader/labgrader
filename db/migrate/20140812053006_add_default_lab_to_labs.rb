class AddDefaultLabToLabs < ActiveRecord::Migration
  def change
    add_column :labs, :default_lab, :int
  end
end
