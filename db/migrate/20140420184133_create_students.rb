class CreateStudents < ActiveRecord::Migration
  def change
    create_table :students do |t|
      t.string  :first_name
      t.string  :last_name
      t.integer :nshe_partial
      t.string  :class_id
      t.float   :lab01
      t.float   :lab02
      t.float   :lab03
      t.float   :lab04
      t.float   :lab05
      t.float   :lab06
      t.float   :lab07
      t.float   :lab08
      t.float   :lab09
      t.float   :labs_total
      t.timestamps
    end
  end
end
