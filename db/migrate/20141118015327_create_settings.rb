class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.string :org

      t.timestamps
    end
  end
end
