class AddMoreMaxLabsToLabs < ActiveRecord::Migration
  def change
    add_column :labs, :lab10_max, :integer
    add_column :labs, :lab11_max, :integer
    add_column :labs, :lab12_max, :integer
    add_column :labs, :lab13_max, :integer
    add_column :labs, :lab14_max, :integer
    add_column :labs, :lab15_max, :integer
    add_column :labs, :lab16_max, :integer
    add_column :labs, :lab17_max, :integer
    add_column :labs, :lab18_max, :integer
    add_column :labs, :lab19_max, :integer
    add_column :labs, :lab20_max, :integer
    add_column :labs, :lab21_max, :integer
    add_column :labs, :lab22_max, :integer
    add_column :labs, :lab23_max, :integer
    add_column :labs, :lab24_max, :integer
    add_column :labs, :lab25_max, :integer
    add_column :labs, :lab26_max, :integer
    add_column :labs, :lab27_max, :integer
    add_column :labs, :lab28_max, :integer
    add_column :labs, :lab29_max, :integer
    add_column :labs, :lab30_max, :integer
  end
end
