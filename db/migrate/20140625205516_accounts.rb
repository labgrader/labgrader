class Accounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string  :username
      t.string  :password
      t.integer :nshe_id
      t.integer :privilege
    end
  end
end
