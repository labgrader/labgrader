class CreateLabs < ActiveRecord::Migration
  def change
    create_table :labs do |t|
      t.string    :lab_id
      t.integer   :lab01_max
      t.integer   :lab02_max
      t.integer   :lab03_max
      t.integer   :lab04_max
      t.integer   :lab05_max
      t.integer   :lab06_max
      t.integer   :lab07_max
      t.integer   :lab08_max
      t.integer   :lab09_max
      t.integer   :labs_total_max
      t.timestamps
    end
  end
end
