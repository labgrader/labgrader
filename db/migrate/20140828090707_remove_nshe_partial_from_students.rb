class RemoveNshePartialFromStudents < ActiveRecord::Migration
  def change
    remove_column :students, :nshe_partial, :integer
  end
end
