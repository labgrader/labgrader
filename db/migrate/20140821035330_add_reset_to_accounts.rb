class AddResetToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :reset, :integer
  end
end
