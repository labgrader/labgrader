class RemoveNsheIdFromAccounts < ActiveRecord::Migration
  def change
    remove_column :accounts, :nshe_id, :integer
  end
end
