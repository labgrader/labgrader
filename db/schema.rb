# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141118015327) do

  create_table "accounts", force: true do |t|
    t.string  "username"
    t.string  "password"
    t.integer "privilege"
    t.integer "reset"
    t.string  "class_id"
  end

  create_table "labs", force: true do |t|
    t.string   "lab_id"
    t.integer  "lab01_max"
    t.integer  "lab02_max"
    t.integer  "lab03_max"
    t.integer  "lab04_max"
    t.integer  "lab05_max"
    t.integer  "lab06_max"
    t.integer  "lab07_max"
    t.integer  "lab08_max"
    t.integer  "lab09_max"
    t.integer  "labs_total_max"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "most_recent_lab"
    t.integer  "lab10_max"
    t.integer  "lab11_max"
    t.integer  "lab12_max"
    t.integer  "lab13_max"
    t.integer  "lab14_max"
    t.integer  "lab15_max"
    t.integer  "lab16_max"
    t.integer  "lab17_max"
    t.integer  "lab18_max"
    t.integer  "lab19_max"
    t.integer  "lab20_max"
    t.integer  "lab21_max"
    t.integer  "lab22_max"
    t.integer  "lab23_max"
    t.integer  "lab24_max"
    t.integer  "lab25_max"
    t.integer  "lab26_max"
    t.integer  "lab27_max"
    t.integer  "lab28_max"
    t.integer  "lab29_max"
    t.integer  "lab30_max"
    t.integer  "default_lab"
    t.string   "lab01_title"
    t.string   "lab02_title"
    t.string   "lab03_title"
    t.string   "lab04_title"
    t.string   "lab05_title"
    t.string   "lab06_title"
    t.string   "lab07_title"
    t.string   "lab08_title"
    t.string   "lab09_title"
    t.string   "lab10_title"
    t.string   "lab11_title"
    t.string   "lab12_title"
    t.string   "lab13_title"
    t.string   "lab14_title"
    t.string   "lab15_title"
    t.string   "lab16_title"
    t.string   "lab17_title"
    t.string   "lab18_title"
    t.string   "lab19_title"
    t.string   "lab20_title"
    t.string   "lab21_title"
    t.string   "lab22_title"
    t.string   "lab23_title"
    t.string   "lab24_title"
    t.string   "lab25_title"
    t.string   "lab26_title"
    t.string   "lab27_title"
    t.string   "lab28_title"
    t.string   "lab29_title"
    t.string   "lab30_title"
  end

  create_table "settings", force: true do |t|
    t.string   "org"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "students", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "class_id"
    t.float    "lab01"
    t.float    "lab02"
    t.float    "lab03"
    t.float    "lab04"
    t.float    "lab05"
    t.float    "lab06"
    t.float    "lab07"
    t.float    "lab08"
    t.float    "lab09"
    t.float    "labs_total"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "lab10"
    t.float    "lab11"
    t.float    "lab12"
    t.float    "lab13"
    t.float    "lab14"
    t.float    "lab15"
    t.float    "lab16"
    t.float    "lab17"
    t.float    "lab18"
    t.float    "lab19"
    t.float    "lab20"
    t.float    "lab21"
    t.float    "lab22"
    t.float    "lab23"
    t.float    "lab24"
    t.float    "lab25"
    t.float    "lab26"
    t.float    "lab27"
    t.float    "lab28"
    t.float    "lab29"
    t.float    "lab30"
    t.string   "email"
  end

end
