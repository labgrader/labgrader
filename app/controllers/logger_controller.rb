class LoggerController < ApplicationController
  before_action :loginCheck, :privilegeCheck, :admin, :sessionCheck

  def index
    @Class_id = params[:Class_id]

    if params[:labNo].present?
      @labNo = params[:labNo]
    else
      @labNo = Labs.first.default_lab
    end

    @lock = params[:lock] == "locked"

    if @Class_id.present?
      @students = Student.find_by_class_id(@Class_id)
    
      if !@students
        flash.now[:notice] = "Error: Student with Class ID '" + @Class_id + "' does not exist"
        @Class_id = nil
      else
        info = Array.new
        info.push "Name: "+@students.first_name+" "+@students.last_name
        info.push "NSHE ID:"+@students.class_id
        flash.now[:notice] = info.join("<br>").html_safe
      end
    end

    begin
      if @labNo
       lab_name = Labs.first.lab_to_str(@labNo.to_i)
       max_score = Labs.first.send(lab_name)
       @scores = (0..max_score).to_a.reverse!
      end
    rescue
      flash.now[:notice] = "Error: lab " + @labNo.to_s + " does not exist"
    end

  end
 
  def save_grade
    cid = params[:Class_id]
    labNo = params[:labNo]
    scoreNo = params[:scoreNo]
    current_student = Student.find_by_class_id(cid)
    labNoString = current_student.lab_to_str(labNo.to_i)

    if current_student != nil
      current_student.update_attribute(labNoString, scoreNo)
      current_student.save
    end
     
    notice  = "Student: " + current_student.first_name + " " + current_student.last_name +
              "<br>" + "Lab: " + labNo.to_s +
              "<br>" + "Score: " + scoreNo.to_s

    flash[:notice] = notice.html_safe

    #if locked stay on current student
    if params[:lock] == "locked"  
      redirect_to :controller => :logger, :action => :index, :Class_id => cid, :labNo => nil, :lock => "locked"
    else
      #currently set to redirect to logger hopefully can make it return to
      # qr reader app when we get around to it
      redirect_to logger_path
    end
  end
end
