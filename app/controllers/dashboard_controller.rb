class DashboardController < ApplicationController
  before_action :loginCheck, :privilegeCheck, :admin, :sessionCheck
  helper_method :sort_column, :sort_direction
  
  def index

    @students = Student.order(sort_column + ' ' + sort_direction)
    @labs = Labs.first

    #allows user to create users dynamically
    if params[:FirstName].present? && params[:LastName].present?
      student = Student.create(first_name: params[:FirstName],
                               last_name: params[:LastName],
                               class_id: params[:classID],
                               email: params[:Email]
                               )
      flash[:notice] = "Student number already used." if student.errors.any?
      redirect_to :back and return


    # Used for exporting spreadsheets
    respond_to do |format|
      format.html
      format.csv { send_data @students.as_csv }
      format.xls 
    end

    #update student
    elsif params[:firstName].present? && params[:lastName].present? && params[:classId].present?
      change = Student.find(params[:id])
      change.update(first_name: params[:firstName],
                    last_name: params[:lastName],
                    class_id: params[:classId],
                    email: params[:email],
                    lab01:params[:lab01],lab02:params[:lab02],lab03:params[:lab03],lab04:params[:lab04],lab05:params[:lab05],
                    lab06:params[:lab06],lab07:params[:lab07],lab08:params[:lab08],lab09:params[:lab09],lab10:params[:lab10],
                    lab11:params[:lab11],lab12:params[:lab12],lab13:params[:lab13],lab14:params[:lab14],lab15:params[:lab15],
                    lab16:params[:lab16],lab17:params[:lab17],lab18:params[:lab18],lab19:params[:lab19],lab20:params[:lab20],
                    lab21:params[:lab21],lab22:params[:lab22],lab23:params[:lab23],lab24:params[:lab24],lab25:params[:lab25],
                    lab26:params[:lab26],lab27:params[:lab27],lab28:params[:lab28],lab29:params[:lab29],lab30:params[:lab30]
      )
      change.save
      flash[:notice] = "Successfully changed."
      redirect_to :back and return


    end

  end

  def title
    #update title

    title = Labs.first
    title.update(
        lab01_title:params[:lab01_title],lab02_title:params[:lab02_title],lab03_title:params[:lab03_title],lab04_title:params[:lab04_title],lab05_title:params[:lab05_title],
        lab06_title:params[:lab06_title],lab07_title:params[:lab07_title],lab08_title:params[:lab08_title],lab09_title:params[:lab09_title],lab10_title:params[:lab10_title],
        lab11_title:params[:lab11_title],lab12_title:params[:lab12_title],lab13_title:params[:lab13_title],lab14_title:params[:lab14_title],lab15_title:params[:lab15_title],
        lab16_title:params[:lab16_title],lab17_title:params[:lab17_title],lab18_title:params[:lab18_title],lab19_title:params[:lab19_title],lab20_title:params[:lab20_title],
        lab21_title:params[:lab21_title],lab22_title:params[:lab22_title],lab23_title:params[:lab23_title],lab24_title:params[:lab24_title],lab25_title:params[:lab25_title],
        lab26_title:params[:lab26_title],lab27_title:params[:lab27_title],lab28_title:params[:lab28_title],lab29_title:params[:lab29_title],lab30_title:params[:lab30_title]
    )
    title.save
    flash[:notice] = "Successfully changed."
    redirect_to :back and return
  end


  def mass_qr
    Student.all.each { |student| UserMailer.welcome_email(student).deliver if student.email }
    redirect_to :back, notice: "Successfully Sent Email To All Students"
  end

  def remove_student
    Student.find(params[:id]).destroy
    redirect_to :back, notice: "Successfully Removed"
  end

  # Used to import spreadsheets
  def import
    begin
      Student.import(params[:file])
      redirect_to :back, notice: "Successfully Imported"
    rescue
      redirect_to :back, notice: "File must be .csv .xls or .xlsx"
    end
  end

  # Show the next available column
  def add_col
    begin
      lab = Labs.first

      if lab.most_recent_lab > 29
        redirect_to :back, notice: "Error: Reached Maximum Number of Labs"
        return
      end

      add_max_score = params[:add_max_score].to_i
      add_max_score = 10 if add_max_score < 1

      lab.most_recent_lab += 1      
      lab_name = lab.curr_lab_to_str 
      lab.send(lab_name + "=", add_max_score) 
      lab.save

      redirect_to :back, notice: "New Lab Added With Max Score: " + add_max_score.to_s 
    rescue
      redirect_to :back, notice: "Error: Could Not Create Lab"
    end
  end

  # Remove any columnn
  def remove_col
    begin
      lab = Labs.first
      remove_lab_num = lab.most_recent_lab
      remove_lab_num = params[:remove_lab_num].to_i if params[:remove_lab_num] != ""

      if lab.most_recent_lab < 1
        redirect_to :back, notice: "Error: No Labs To Remove"
        return
      elsif remove_lab_num < 1 || remove_lab_num > lab.most_recent_lab
        redirect_to :back, notice: "Error: Lab " + remove_lab_num.to_s + " does not exist."
        return
      end
     
      # Clears any record of to be removed lab
      Student.all.each do |student|
        student.send(student.lab_to_str(remove_lab_num) + "=", nil)
        student.save
      end

      if remove_lab_num == lab.most_recent_lab 
        lab_name = lab.curr_lab_to_str
        lab.send(lab_name + "=", nil)
        lab.most_recent_lab -= 1
      else
        lab_name = lab.lab_to_str(remove_lab_num)
        lab.send(lab_name + "=", nil)
      end
     
      # Checking to see if any earlier labs are nil
      lab_name = lab.curr_lab_to_str
      until lab.most_recent_lab < 1 || lab.send(lab_name) != nil do
        lab.most_recent_lab -= 1
        lab_name = lab.curr_lab_to_str
      end
      
      lab.save
        
      redirect_to :back, notice: "Removed Lab: " + remove_lab_num.to_s
     rescue
      redirect_to :back, notice: "Error: Could Not Remove Lab"
    end
  end

  def set_default_lab
    default_num = params[:default_num].to_i
    lab = Labs.first
    lab_name = lab.lab_to_str(default_num)

    if params[:default_num] == ""
      lab.default_lab = lab.most_recent_lab
    elsif default_num > 0 && default_num <= lab.most_recent_lab && lab.send(lab_name)
      lab.default_lab = default_num
    else
      redirect_to :back, notice: "Cannot Set Lab " + params[:default_num] + " As Default Lab"
      return
    end

    lab.save
    redirect_to :back, notice: "Lab " + lab.default_lab.to_s + " Set As Default Lab"
  end

  private
  def sort_column
    Student.column_names.include?(params[:sort]) ? params[:sort] : "last_name"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end

end 
