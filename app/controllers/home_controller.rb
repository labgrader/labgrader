class HomeController < ApplicationController
  before_action :admin, :sessionCheck

  def index
    @account = Account.all

    #prints qr code to page
    if session[:username] != nil
      if Account.find_by_username(session[:username]) != nil
      @student = Student.find_by_class_id(Account.find_by_username(session[:username]).class_id)
      end
      if @student != nil
        #keep qr code tied to class id so no sensitive information can be taken from code
        @qr = RQRCode::QRCode.new(url_for(controller: 'logger') +'?Class_id='+
                                  @student.class_id.to_s,
                                  size: 4,
                                  level: :l)
        @png = @qr.to_img.resize(250,250)
        @labs2 = Labs.first
      end
    end

    if params[:account].present? && params[:password].present? && params[:password2].present?
      #deal with all the error messsages for incorrect form fills
      errors = Array.new
      if Student.find_by_class_id(params[:class_id].to_i) == nil
        errors.push "Not a recognized student number."
      end
      if Account.find_by_class_id(params[:class_id].to_i) != nil
        errors.push "Student number already used."
      end
      if Account.find_by_username(params[:account].downcase) != nil
        errors.push "Username is already used."
      end
      if params[:password].length < 6
        errors.push "Passwords is too short, 1-6 characters."
      end
      if params[:password] != params[:password2]
        errors.push "Passwords do not match."
      end
      
      if !errors.empty?
        flash[:notice] = errors.join("<br>").html_safe
      else
        #creates account if form is completed properly
          accounts = Account.create(username: params[:account].downcase,
                                 password: BCrypt::Password.create(params[:password]),
                                 privilege: 0,
                                 reset: 0,
                                 class_id: params[:class_id].to_i)

        errors = "<font color='green'> Account successfully created. </font>"
        flash[:notice] = errors.html_safe
      end
      redirect_to home_path
    end
  end

  def about
  end

  def qr
    #currently using nshe, maybe change to more intuitive name later
    @student = Student.find_by_class_id(params[:Class_id])
    if @student != nil
      #keep qr code tied to class id so no sensitive information can be taken from code
      @qr = RQRCode::QRCode.new(url_for(controller: 'logger') +'?Class_id='+ 
                                  @student.class_id.to_s,
                                  size: 4,
                                  level: :l)
      @png = @qr.to_img.resize(250,250)
    else
      if params[:Class_id].present?
        flash[:notice] = "Student " + params[:Class_id] + " does not exist"
      else
        flash[:notice] = "You did not enter a student id."
      end
      redirect_to :back
    end
  rescue
    redirect_to root_path
  end
end
