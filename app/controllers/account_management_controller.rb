class AccountManagementController < ApplicationController
   before_action :loginCheck, :admin, :sessionCheck

   def index
     if params[:CreateUsername] != nil
      if Account.find_by_username(params[:CreateUsername].downcase) == nil
       Account.create(:username=>params[:CreateUsername].downcase,:password=>BCrypt::Password.create(params[:CreatePassword]),:privilege=>params[:CreatePrivilege],:reset=>0)
      else
       flash[:notice]="Username is already used."
       redirect_to account_management_path
      end
     end 

    if params[:delete].present?
       Account.find_by_username(params[:username]).destroy
       flash[:notice]="Account deleted successfully."
    end

     @accounts = Account.all
      @user = Account.find_by_username(session[:username])
      if params[:username].present?
        temp = Account.find_by_username(params[:username])
         
        if @user != nil && @user.privilege == 1 && temp != nil
          if temp.password != params[:password]
            temp.password = BCrypt::Password.create(params[:password])
            temp.reset = 0
          end
          temp.privilege = params[:privilege]
        end

       if @user != nil && @user.privilege == 0
         if params[:new_password] == params[:confirm_password] && BCrypt::Password.new(temp.password) == params[:old_password]
           temp.password = BCrypt::Password.create(params[:new_password])
           flash[:notice] = "Successfully changed password."
         else
           flash[:notice] = "Password error. Make sure you entered original password correctly, and the new password fields match."
         end
       end
       
      if temp != nil
        temp.save
      end
        redirect_to account_management_path
      end 
   end

end
