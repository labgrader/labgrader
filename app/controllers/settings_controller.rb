class SettingsController < ApplicationController
  
  def index
    @act = Account.find_by_username('admin')
    
    if params[:org]
      @org = params[:org]
      @act.setting.org = @org
      @act.setting.save
    end
  end
end
