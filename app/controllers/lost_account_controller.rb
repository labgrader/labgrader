class LostAccountController < ApplicationController
before_filter :test, :sessionCheck, only:[:index]
   def index
     if params[:first_name].present?
       temp = Student.find_by_class_id(params[:class_id])
       if temp != nil
           if temp.last_name.downcase == params[:last_name].downcase && temp.first_name.downcase == params[:first_name].downcase
               temp2 = Account.find_by_nshe_id(params[:class_id])
               temp2.reset = 1
               temp2.save
               flash[:notice] = "Request has been sent."
           end
      else
          flash[:notice] = "Could not find this student."
      end
      redirect_to lost_account_path      
     end

  end

  def test
     if session[:username] != nil
        flash[:notice] = "You are already logged in."
        redirect_to root_path
     end  
  end

end
