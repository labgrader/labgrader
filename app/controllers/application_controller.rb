class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  before_filter :set_cache_buster

  def set_cache_buster
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"] = "no-cache"
    response.headers["Expires"] = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def login
    if params[:username].present?
      username = params[:username].downcase
      password = params[:password]
      temp = Account.find_by_username(username)

      if temp != nil && BCrypt::Password.new(temp.password) == password
        reset_session
        session[:username] = temp.username   #{:value => @username, :expires => Time.now + 60}
      else
        flash[:notice] = "Incorrect login information."
      end
    end
      redirect_to root_url
  end

  def logout
    reset_session
    redirect_to root_url
  end

  protected
  def admin
    # This is our super secret admin section
    #num_of_admin = 0
    #Account.all.each { |account| num_of_admin += 1 if account.privilege == 1 && account.username != "admin" }

    #if num_of_admin == 0
    if !Account.first
      Account.create(username: "admin", password: BCrypt::Password.create("password"), privilege: 1, reset: 0)
      Account.find_by_username('admin').create_setting
    end
    # else
    #  Account.find_by_username("admin").destroy if Account.find_by_username("admin") && num_of_admin > 0
    #end

    Labs.create(most_recent_lab:0, default_lab:0) if !Labs.first
  end

  def loginCheck
    if session[:username] == nil
      flash[:notice] = "You need to login."
      redirect_to root_url
    end
  end

  def privilegeCheck
    temp2 = Account.find_by_username(session[:username])
    if temp2 != nil
      if temp2.privilege != 1
        flash[:notice] = "You do not have privileges to view this page."
        redirect_to root_url
      end
    end
  end

  def sessionCheck
    if Account.find_by_username(session[:username]) == nil && session[:username].present?
      reset_session
    end
  end



end
