class EmailerController < ApplicationController
  def sendmail
    UserMailer.welcome_email(Student.find(params[:id])).deliver
    return if request.xhr?
    redirect_to :back, notice: "Successfully Sent Email"
  end
end
