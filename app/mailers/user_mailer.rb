class UserMailer < ActionMailer::Base
  default from: 'egg101labserver@cs.unlv.edu'

  def welcome_email(user)
    if user != nil && user.email != nil 
      qr = RQRCode::QRCode.new('egg101labserver.cs.unlv.edu/egg/logger?Class_id=' + 
                              user.class_id.to_s,
                              size: 4,
                              level: :l)

      png = qr.to_img.resize(250, 250)
      io = StringIO.new
      png.write(io)
      io.rewind

      attachments['qrcode.png'] = {
        mime_type: 'image/png',
        content: io.read
      }

      @user = user
      mail(to: @user.email, subject: 'Welcome to the Labgrader')
    end
  end
end
