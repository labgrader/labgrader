class Labs < ActiveRecord::Base
  attr_accessible :lab01_max, :lab02_max, :lab03_max, :lab04_max, :lab05_max, :lab06_max, :lab07_max, :lab08_max, :lab09_max, :lab10_max, 
                  :lab11_max, :lab12_max, :lab13_max, :lab14_max, :lab15_max, :lab16_max, :lab17_max, :lab18_max, :lab19_max, :lab20_max, 
                  :lab21_max, :lab22_max, :lab23_max, :lab24_max, :lab25_max, :lab26_max, :lab27_max, :lab28_max, :lab29_max, :lab30_max,
                  :lab01_title, :lab02_title, :lab03_title, :lab04_title, :lab05_title, :lab06_title, :lab07_title, :lab08_title, :lab09_title, :lab10_title,
                  :lab11_title, :lab12_title, :lab13_title, :lab14_title, :lab15_title, :lab16_title, :lab17_title, :lab18_title, :lab19_title, :lab20_title,
                  :lab21_title, :lab22_title, :lab23_title, :lab24_title, :lab25_title, :lab26_title, :lab27_title, :lab28_title, :lab29_title, :lab30_title,

      :most_recent_lab, :default_lab


  # Returns the string of the most recent lab in the format of labxx_max
  def curr_lab_to_str    
    return "lab0" + self.most_recent_lab.to_s + "_max" if self.most_recent_lab < 10 
    return "lab" + self.most_recent_lab.to_s + "_max"
  end

  def lab_to_str(num)
    return "lab0" + num.to_s + "_max" if num < 10
    return "lab" + num.to_s + "_max"
  end
  def lab_title_to_str(num)
    return "lab0" + num.to_s + "_title" if num < 10
    return "lab" + num.to_s + "_title"
  end
end
