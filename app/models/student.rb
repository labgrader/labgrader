class Student < ActiveRecord::Base
  attr_accessible :first_name, :last_name, :class_id, :email,
                  :lab01, :lab02, :lab03, :lab04, :lab05, :lab06, :lab07, :lab08, :lab09, :lab10,
                  :lab11, :lab12, :lab13, :lab14, :lab15, :lab16, :lab17, :lab18, :lab19, :lab20,
                  :lab21, :lab22, :lab23, :lab24, :lab25, :lab26, :lab27, :lab28, :lab29, :lab30

                  validates :class_id, :presence => false, :uniqueness => true

  def lab_to_str(num)
    return "lab0" + num.to_s if num < 10
    return "lab" + num.to_s
  end
   
  # This method is used to export the spreadsheet to CSV format  
  def self.as_csv
    CSV.generate do |csv|
      csv << column_names
      all.each do |student|
        csv << student.attributes.values_at(*column_names)
      end
    end
  end

  # Hashes info from the spreadsheet into the database
  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      student = find_by_class_id(row["class_id"]) || new
      student.attributes = row.to_hash.slice(*accessible_attributes)
      #student.attributes = row.to_hash.slice(:first_name, :last_name, :nshe_partial, :class_id)
      student.save
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::Csv.new(file.path)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

end
