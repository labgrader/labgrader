class Account < ActiveRecord::Base
  has_one :setting, :foreign_key => 'id'
  attr_accessible :username, :password, :privilege, :class_id, :reset
  validates_uniqueness_of :username
end
